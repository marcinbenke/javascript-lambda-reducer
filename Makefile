
OUT=out/f.js
GRAMMAR=grammar.txt
GRAMMAR_OUT=parser.js

SUBTERMS=NoOp OpP Int AbsTerm LetAbbr AppTerm FreeVar FunTerm StrTerm BoundVar Sentinel
OTHER=Utility parser Term html 




SUBTERM_FILES=$(addprefix src/subterms/, $(addsuffix .js, $(SUBTERMS)))
OTHER_FILES=$(addprefix src/, $(addsuffix .js, $(OTHER)))
GRAMMAR_FILE=$(addprefix src/, $(GRAMMAR))
GRAMMAR_OUT_FILE=$(addprefix src/, $(GRAMMAR_OUT))

FILES=$(SUBTERM_FILES) $(OTHER_FILES)

$(OUT): $(FILES)
	java -jar  ~/Dropbox/bin/compiler.jar --js $(FILES) --process_jquery_primitives --js_output_file=$(OUT) --externs extern.js
$(GRAMMAR_OUT_FILE): $(GRAMMAR_FILE) 
	jison $(GRAMMAR_FILE) -o $(GRAMMAR_OUT_FILE)
debug:
	cat $(FILES) > $(OUT)

.PHONY: open clean
open:
	open out/reduce.html
clean:
	rm $(GRAMMAR_OUT_FILE) $(OUT)

