	var $main,$debug,$normresult,$input;

	function setLimit () {
		var value = $("#normalizelimit").val();
		if (! /^\d+$/.test(value)) {
			$normresult.text("Please enter a positive integer");
			return false;
		}
		Utility.normalizeLimit = value;
		return true;
	}

	function reset () {
		var term = $main.data("term");
		if (term) {
			term.destroy();
			$main.data("term",null);
			$main.empty();
			Semantic.contract = [];
		}
	}
	
	$(document).on("click", "#newterm", function () {
		$debug.empty();
		$normresult.empty();
		
		reset();

		try {
			placeTerm(parser.parse($input.val()));
		} catch (e) {
			$debug.text(e);
		}
	});
	$(document).on("click", "#normalize", function () {
		var term = $main.data("term");
		if (! term) {
			return false;
		}
		if (! setLimit()) return false;
		var result = term.normalize();
		if (result)
			$normresult.text("Term normalized");
		else
			$normresult.text("Not normalized");	
		placeTerm(term);
	});
	$(document).on("click", "#develop", function () {
		var term = $main.data("term");
		if (! term) {
			return false;
		}
		$normresult.empty();
		term.develop();
		placeTerm(term);
	});
	$(document).on("click", "#preprocess", function () {
		var term = $main.data("term");
		if (! term) {
			return false;
		}
		if (! setLimit()) return false;
		$normresult.empty();
		term.preprocess();
		placeTerm(term);
	});
	$(document).on("keydown", "textarea", function(e) {
		if(e.keyCode === 9) {
			var start = this.selectionStart;
			var end = this.selectionEnd;

			var $this = $(this);
			var value = $this.val();

			$this.val(value.substring(0, start)
						+ "\t"
						+ value.substring(end));

			this.selectionStart = this.selectionEnd = start + 1;

			e.preventDefault();
		}
	});
	$(function() {
		$main = $("#main");
		$debug = $("#debug");
		$input = $("#termtextarea");
		$normresult = $("#normalresult");

		$("#newterm").click();
		
		$("#normalizelimit").on("keyup", function (e) {
			if (e.which == 13) {
				$("#normalize").click();
				return false;
			}
		});
		$main.on("mouseover", ".bindingspot", function(e) {
			var $target = $(e.target);
			var redex = $target.data("redex");

			redex.illuminate();

			return false;
		});
		$main.on("mouseleave", ".bindingspot", function(e) {
			var $target = $(e.target);
			var redex = $target.data("redex");

			redex.unilluminate();
			
			return false;
		});
		$main.on("click", ".bindingspot", function (e) {
			var $target = $(e.target);
			var redex = $target.data("redex");
			var par = redex.parent;

			var $old_location = par.display;

			redex.contractRedex();

			if ($old_location) {
				par.makeJQueryHTMLObject();
				$old_location.replaceWith(par.display);
			} else {
				placeTerm($("#main").data("term"));
			}
			e.preventDefault();
		});
	});
	function semanticAnalysis (term) {
		// dummy for now.
		return term;
	}
	function placeTerm (s) {
		$main.empty()
		     .append(s.makeJQueryHTMLObject())
			 .data("term", s);

	}

