function AbsTerm (boundVars, term, varString) {
	this.depth = 0;
	this.display = null;

	this.boundVars = []; // will be registered by BoundVar constructor
	varString = varString || "_";
	this.term = term;
	this.name = varString;

	// Register parenthood in children.
	term.parent = this;

	// We turn free variables into bound variables.
	for (var i = 0; i < boundVars.length; ++i) {
		var varToChange = boundVars[i];
		var boundVar = new BoundVar(this);
		var par = varToChange.parent;
		par.replaceChild(varToChange,boundVar);
	}

}
AbsTerm.prototype = {
	constructor: AbsTerm,
/* Subterm */
	type: function () { 
		return Type.AbsTerm; 
	},
	children: function () {
		return [this.term];
	},
	getName: function () {
		return this.boundVars.length ? this.name : "_";
	},
	cloneWithSideEffects: function () {
		var backup = this.boundVars;
		var name = this.getName();

		this.boundVars = [];
		var cloneTerm = this.term.cloneWithSideEffects();
		var ret = new AbsTerm(this.boundVars, cloneTerm, name);

		this.boundVars = backup;
		return ret;
	},
	replaceChild: function (child, newChild) {
		if (this.term === child) {
			//child.parent = null;
			this.term = newChild;
			newChild.parent = this;
		} else {
			throw new InvalidArguments("Method not invoked on parent of child");
		}
	},
	hardDestroy: function () {
		this.parent = null;
		this.display = null;

		this.term.hardDestroy();

		this.term = null;
		this.boundVars = null;
	},
	softDestroy: function () {
		this.parent = null;
		this.display = null;

		this.term.softDestroy();
	},
	makeJQueryHTMLObject: function () {
		var par = this.parent;
		this.depth = par.depth + 1;

		var $span = $("<span>");

		this.name = Utility.getUnblemishedName(this.getName());
		
		// Here we make the binding location
		var $binder = $("<span>");
		$binder.append("\u03BB" + this.getName());
		// This is the number making bound variables unambiguous.
		// Note: it is not part of the binding location. 

		if (this.name != "_") {
			var $number = $("<sub>");
			$number.text(this.depth);
			$span.append($binder,$number, '.', this.term.makeJQueryHTMLObject());
		}
		else
			$span.append($binder, '.', this.term.makeJQueryHTMLObject());

		// This is so we can highlight the redex on mouseover. 
		// The binding spot keeps the same kind of data as its parent.
		if (par.type() == Type.AppTerm && par.left === this) {
			$binder.addClass("bindingspot");
			$binder.data("redex", this.parent);
		}

		this.display = $span;
		$span.data("term",this);
		return $span;	
	},
	isRedex: function () {
		return false;
	},
	canApply: function (toTerm) {
		return true;
	},
/* Applyable */
	applyTo: function (substituent) {
		var applicant = this;
		var contextHoles = applicant.boundVars;
		var hole;
		if (contextHoles && contextHoles.length) {
			// I redex
			hole = contextHoles.shift();
			// Use the real one first
			hole.parent.replaceChild(hole, substituent);
			while (contextHoles.length) {
				hole = contextHoles.shift();
				//For addition ones, make a copy.
				var clone = substituent.cloneWithSideEffects();
				hole.parent.replaceChild(hole,clone);
			}
		} else { 
			// K-redex
			// destroy the substituent part of the redex.
			substituent.hardDestroy();
		}
		this.parent.parent.replaceChild(this.parent, applicant.term);
		return true;
	}
}
