function BoundVar (boundLoc) {
	this.depth = 0;
	this.display = null;
	this.boundAt = boundLoc;
	boundLoc.boundVars.push(this);
}
BoundVar.prototype = {
	constructor: BoundVar,
/* Subterm */
	type: function () {
		return Type.BoundVar;
	},
	children: function () {
		return [];
	},
	getName: function () {
		return this.boundAt.name;
	},
	cloneWithSideEffects: function () {
		return new BoundVar(this.boundAt)
	},
	replaceChild: function (child, newChild) {
		throw new Error("No Child");
	},
	hardDestroy: function () {
		this.parent = null;
		this.display = null;
		var boundLocVars = this.boundAt.boundVars;
		var index = boundLocVars.indexOf(this);
		boundLocVars.splice(index,1);
	},
	softDestroy: function () {
		this.parent = null;
		this.display = null;
	},
	makeJQueryHTMLObject: function () {
		this.depth = this.parent.depth + 1;

		var $span = $("<span>");

		var $number = $("<sub>");
		$number.text(this.boundAt.depth);

		$span.append(this.getName(), $number);

		$span.data("term",this);
		this.display = $span;
		return $span;	
	},
	isRedex: function () {
		return false;
	},
	canApply: function (toTerm) {
		return false;
	}
}
