
function RefCount (num) {
	this.n = num;
}
RefCount.prototype = {
	constructor: RefCount,
	incr: function () {
		++this.n;
	},
	decr: function () {
		--this.n;
	},
	valueOf: function () {
		return this.n;
	}
}

function LetAbbr (name, abbr, refcounter) {
	this.depth = 0;
	this.display = null;

	this.name = name;
	this.abbr = abbr;
	if (refcounter) {
		this.refcounter = refcounter;
		refcounter.incr();
	} else
		this.refcounter = new RefCount(1);
}



LetAbbr.prototype = {
	constructor: LetAbbr,
/* Subterm */
	type: function () {
		return Type.LetAbbr;
	},
	children: function () {
		return [];
	},
	getName: function () {
		return this.name;
	},
	cloneWithSideEffects: function () {
		return new LetAbbr(this.name, this.abbr, this.refcounter);
	},
	replaceChild: function (child, newChild) {
		if (this.abbr === child) {
			//child.parent = null;
			this.abbr = newChild;
			newChild.parent = this;
		} else {
			throw new InvalidArguments("Method not invoked on parent of child");
		}	
	},
	hardDestroy: function () {
		this.parent = null;
		this.display = null;
		
		this.refcounter.decr();

		if (! (+this.refcounter))
			this.abbr.hardDestroy();
	},
	softDestroy: function () {
		this.parent = null;
		this.display = null;
		
		this.abbr = null;
	},
	makeJQueryHTMLObject: function () {
		this.depth = this.parent.depth + 1;

		var $span = $("<span>");
		var name = Utility.getUnblemishedName(this.name);
		var $span = $span.append(name);

		$span.addClass("bindingspot");
		$span.data("redex", this);

		this.display = $span;
		$span.data("term",this);
		return $span;	
	},
	isRedex: function () {
		return true;
	},
	canApply: function () {
		return false; // not able to accept input
	},
/* Redex */
	isPreproccessed: function () {
		return true;
	},
	contractRedex: function () {
		if (this.refcounter > 1)
			this.parent.replaceChild(this,this.abbr.child.cloneWithSideEffects());
		else
			this.parent.replaceChild(this,this.abbr.child);
	},
	illuminate: function () {
		this.display.addClass("abbreviationredex");
	},
	unilluminate: function () {
		this.display.removeClass("abbreviationredex");
	}
}

