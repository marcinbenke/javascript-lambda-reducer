
var Operations = {
	IPlus : 1,
	ITimes : 2,
	IDivides : 3,
	IEquals : 4,
	IMinus: 5
}


function OpP (op,value) {
	this.depth = 0;
	this.display = null;

	this.value = value;
	this.op = op;
}
OpP.prototype = {
	constructor: OpP,
/* Subterm */
	type: function () {
		return Type.OpP;
	},
	children: function () {
		return [];
	},
	getName: function () {
		return this.value.toString();
	},
	cloneWithSideEffects: function () {
		return new OpP(this.op,this.value);
	},
	replaceChild: function (child, newChild) {
		throw new NoChild();
	},
	hardDestroy: function () {
		this.parent = null;
		this.display = null;
	},
	softDestroy: function () {
		this.parent = null;
		this.display = null;
	},
	makeJQueryHTMLObject: function () {
		this.depth = this.parent.depth + 1;

		var $span = $("<span>");
		switch (this.op) {
			case Operations.IPlus:
				$span.append("+");
				break;
			case Operations.IMinus:
				$span.append("-");
				break;
			case Operations.ITimes:
				$span.append("*");
				break;
			case Operations.IDivides:
				$span.append("/");
				break;
			case Operations.IEquals:
				$span.append("==");
				break;
			default:
				return false;
		}
		if (this.value >= 0)
			$span.append("<sub>" + this.value + "</sub>");

		if (this.parent.isRedex() && this.parent.left == this) {
			$span.addClass("bindingspot");
			$span.data("redex", this.parent);
		}	

		this.display = $span;
		$span.data("term",this);
		return $span;	
	},
	isRedex: function () {
		return false;
	},
	canApply: function (other) {
		switch (this.op) {
			case Operations.IPlus:
			case Operations.ITimes:
			case Operations.IDivides:
			case Operations.IEquals:
			case Operations.IMinus:
				return other.type() == Type.Int;
			default:
				return false;
		}
	},
/* Applyable */
	applyTo: function (number) {
		var replaceWith;
		if (this.value >= 0) {
			switch (this.op) {
				case Operations.IPlus:
					replaceWith = new Int( (+this.value) + (+number.value));
					break;
				case Operations.IMinus:
					var val = (+this.value) - (+number.value);
					replaceWith = new Int( val < 0 ? 0 : val );
					break;
				case Operations.ITimes:
					replaceWith = new Int( (+this.value) * (+number.value));
					break;
				case Operations.IDivides:
					replaceWith = new Int( ((+this.value) / (+number.value)) | 0 );
					break;
				case Operations.IEquals:
					if ( (+this.value) == (+number.value) )
						replaceWith = Term.constant.$true.head.child.cloneWithSideEffects();
					else
						replaceWith = Term.constant.$false.head.child.cloneWithSideEffects();
					break;
				default:
					return false;
			}
		} else {
			replaceWith = this;
			this.value = number.value;
		}
		this.parent.parent.replaceChild(this.parent, replaceWith);
	}
}


