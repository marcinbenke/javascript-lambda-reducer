function StrTerm (value) {
	this.depth = 0;
	this.display = null;

	this.value = value;
}
StrTerm.prototype = {
	constructor: StrTerm,
/* Subterm */
	type: function () {
		return Type.StrTerm;
	},
	children: function () {
		return [];
	},
	getName: function () {
		return '"' + this.value + '"';
	},
	cloneWithSideEffects: function () {
		return new StrTerm(this.value);
	},
	replaceChild: function (child, newChild) {
		throw new NoChild();
	},
	hardDestroy: function () {
		this.parent = null;
		this.display = null;
	},
	softDestroy: function () {
		this.parent = null;
		this.display = null;
	},
	makeJQueryHTMLObject: function () {
		this.depth = this.parent.depth + 1;

		var $span = $("<span>");
		$span.append('"' + this.value + '"');

		if (this.parent.isRedex() && this.parent.left == this) {
			$span.addClass("bindingspot");
			$span.data("redex", this.parent);
		}

		this.display = $span;
		$span.data("term",this);
		return $span;	
	},
	isRedex: function () {
		return false;
	},
	canApply: function (other) {
		return other.type() == Type.StrTerm;
	},
/* Applyable */
	applyTo: function (otherString) {
		// assert: canApply == true
		this.value = this.value + otherString.value; 
		this.parent.parent.replaceChild(this.parent, this);
	}
}

