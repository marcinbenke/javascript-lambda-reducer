function AppTerm (left, right) {
	this.depth = 0;
	this.display = null;

	this.left = left;
	this.right = right;

	// Register parenthood in children.
	left.parent = this;
	right.parent = this;
}
AppTerm.prototype = {
	constructor: AppTerm,
/* Subterm */
	type: function () { 
		return Type.AppTerm;
	},
	children: function () {
		return [this.right, this.left];
	},
	getName: function () {
		throw new Error("Invalid Type");
	},
	cloneWithSideEffects: function () {
		var newLeft = this.left.cloneWithSideEffects();
		var newRight = this.right.cloneWithSideEffects();
		return new AppTerm(newLeft, newRight);
	},
	replaceChild: function (child, newChild) {
		if (this.left === child) {
			//child.parent = null;
			this.left = newChild;
			newChild.parent = this;
		} else if (this.right === child) {
			//child.parent = null;
			this.right = newChild;
			newChild.parent = this;
		}
		else
			throw new Error("Method not invoked on parent of child");
	},
	hardDestroy: function () {
		this.parent = null;
		this.display = null;

		this.left.hardDestroy();
		this.right.hardDestroy();

		this.left = null;
		this.right = null;
	},
	softDestroy: function () {
		this.parent = null;
		this.display = null;

		this.left.softDestroy();
		this.right.softDestroy();
	},
	makeJQueryHTMLObject: function () {
		var par = this.parent;
		this.depth = par.depth + 1;

		var $span = $("<span>");

		var left = this.left;
		var right = this.right;

		var $left = left.makeJQueryHTMLObject();
		var $right = right.makeJQueryHTMLObject();	

		// The below just ensures that parentheses are not places unless
		// they need to be. So association to the left is used essentially
		// whenever possible.
		var paren = false;
		if (left.type() == Type.AbsTerm) {
			$span.append("(", $left, ")", $("<wbr>"));
			paren = true;
		} else
			$span.append($left);

		if (right.type() == Type.AbsTerm || right.type() == Type.AppTerm)
			$span.append($("<wbr>"),"(", $right, ")");
		else {
			if (! paren)
				$span.append(" ");
			$span.append($right);
		}

		this.display = $span;
		$span.data("term",this);
		return $span;	
	},
	isRedex: function () {
		return this.left.canApply(this.right);
	},
	canApply: function (toTerm) {
		return false;
	},
/* Redex */
	isPreproccessed: function () {
		return false;
	},
	contractRedex: function () { 
		this.left.applyTo(this.right);
	},
	illuminate: function () {
		this.left.display.addClass("lefttermredex");
		this.right.display.addClass("righttermredex");
		if (this.left.type() == Type.AbsTerm) {
			var boundVars = this.left.boundVars;
			for (var i = 0; i < boundVars.length; ++i) {
				boundVars[i].display.addClass("activeboundvar");
			}
		}
	},
	unilluminate: function () {
		this.left.display.removeClass("lefttermredex");
		this.right.display.removeClass("righttermredex");
		var boundVars = this.left.boundVars;
		if (this.left.type() == Type.AbsTerm) {
			for (var i = 0; i < boundVars.length; ++i) {
				boundVars[i].display.removeClass("activeboundvar");
			}
		}
	}
}

