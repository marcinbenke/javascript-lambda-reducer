function FreeVar (varString,registry) {
	this.depth = 0;
	this.display = null;

	this.name = varString;
	this.registry = registry;

	if (! registry[varString])
		registry[varString] = [];
	registry[varString].push(this);
}
FreeVar.prototype = {
	constructor: FreeVar,
/* Subterm */
	type: function () {
		return Type.FreeVar;
	},
	children: function () {
		return [];
	},
	getName: function () {
		return this.name;
	},
	cloneWithSideEffects: function () {
		return new FreeVar(this.name, this.registry);
	},
	replaceChild: function (child, newChild) {
		throw new Error("No Child");
	},
	hardDestroy: function () {
		this.parent = null;
		this.display = null;
		var theseVariables = this.registry[this.name];
		var index = theseVariables.indexOf(this);
		theseVariables.splice(index,1);
		//this.name = null;
		this.registry = null;
	},
	softDestroy: function () {
		this.parent = null;
		this.display = null;
	},
	makeJQueryHTMLObject: function () {
		this.depth = this.parent.depth + 1;

		var $span = $("<span>");
		var name = Utility.getUnblemishedName(this.name);
		var $span = $span.append(name);

		this.display = $span;
		$span.data("term",this);
		return $span;	
	},
	isRedex: function () {
		return false;
	},
	canApply: function () {
		return false; // not able to accept input
	}
}
