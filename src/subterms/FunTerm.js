function FunTerm (name, lambdaList, template) {
	this.depth = 0;
	this.display = null;

	var arity = lambdaList.length;
	var leftmost = new NoOp();
	var underlying = leftmost;
	
	// Build underlying term.
	for (var i = 0; i < lambdaList.length; ++i)
		underlying = new AppTerm(underlying, lambdaList[i]);

	this.arity = arity;
	this.name = name;
	this.templateTerm = template;
	this.underlying = underlying;
	this.leftmost = leftmost;
	this.contractable = false;
	
	this.templateTerm.parent = this;
	this.underlying.parent = this;
}
FunTerm.prototype = {
	constructor: FunTerm,
/* Subterm */
	type: function () {
		return Type.FunTerm;
	},
	children: function () {
		return [this.underlying];
	},
	getName: function () {
		return this.name;
	},
	cloneWithSideEffects: function (registry) {
		var newLambdaList = [];

		var traversing = this.underlying;
		for (var i = 0; i < this.arity; ++i) {
			var copiedSubterm = traversing.right.cloneWithSideEffects(registry);
			newLambdaList.unshift(copiedSubterm);
			traversing = this.underlying.left;
		}
		var newTemplateTerm = this.templateTerm.cloneWithSideEffects(registry);
		var newTerm = new FunTerm(this.name, newLambdaList, newTemplateTerm);
		newTerm.contractable = this.contractable;
		return newTerm;
	},
	replaceChild: function (child, newChild) {
		if (this.underlying === child) {
			// assert: lambdaList.length == 0
			this.underlying = newChild;
			newChild.parent = this;
		} else if (this.templateTerm === child) {
			this.templateTerm = newChild;
			newChild.parent = this;
			this.contractable = true;
		}
		else {
			throw new InvalidArguments("Method not invoked on parent of child")
		}
	},
	hardDestroy: function () {
		this.parent = null;
		this.display = null;

		this.templateTerm.hardDestroy();
		this.underlying.hardDestroy();

		this.lambdaList = null;
	},
	softDestroy: function () {
		this.parent = null;
		this.display = null;

		this.underlying.softDestroy();
	},
	makeJQueryHTMLObject: function () {
		this.depth = this.parent.depth + 1;

		var $span = $("<span>");

		var lamList = this.lambdaList;

		var $name = $("<span>");
		$name.append(this.name);
		if (this.isRedex()) {
			$name.addClass("bindingspot");
			$name.data("redex", this);
		}

		$span.append($name, "(");

		var $instList = $("<span>");
		var traversing = this.underlying;
		this.underlying.depth = this.depth;
		if (this.arity) {
			$instList.append(traversing.right.makeJQueryHTMLObject());
			traversing = traversing.left;
			for (var i = 1; i < this.arity; ++i) {
				$instList.prepend(traversing.right.makeJQueryHTMLObject(),",");
				traversing = traversing.left;
			}
		}
		$span.append($instList, ")");

		this.display = $span;
		$span.data("term",this);
		return $span;	
	},
	isRedex: function () {
		return this.contractable;
	},
	canApply: function () {
		return false; // not able to accept input
	},
/* Redex */
	isPreproccessed: function () {
		return true;
	},
	contractRedex: function () {
		var par = this.leftmost.parent;
		par.replaceChild(this.leftmost, this.templateTerm);
		for (var i = 0; i < this.arity; ++i) {
			if (! par.isRedex())
				break;
			par.contractRedex();
			par = par.parent;
		}
		this.parent.replaceChild(this, this.underlying);
	},
	illuminate: function () {
		this.display.addClass("abbreviationredex");
	},
	unilluminate: function () {
		this.display.removeClass("abbreviationredex");
	}
}
