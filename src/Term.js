function Term (kind) {
	var arg1, arg2;
	arg1 = arguments[1];
	switch (kind) {
		case Type.FreeVar:
			// Blank registry
			var registry = Object.create(null);
			this.freevarRegistry = registry;
			// Make variable and sentinel
			var variable = new FreeVar(arg1, registry);
			this.head = new Sentinel(variable);
			break;
		case Type.AbsTerm:
			arg2 = arguments[2];
			// Make abstraction and sentinel.
			var boundVars = arg2.freevarRegistry[arg1] || [];
			var abstraction = new AbsTerm(boundVars, arg2.head.child, arg1);
			this.head = new Sentinel(abstraction);
			// Steal term's registry
			this.freevarRegistry = arg2.freevarRegistry;
			// Delete the bound variable
			delete this.freevarRegistry[arg1];
			break;
		case Type.AppTerm:
			arg2 = arguments[2];
			var arg1head = arg1.head;
			var arg2head = arg2.head;
			var application = new AppTerm(arg1head.child, arg2head.child);
			this.head = new Sentinel(application);
			// Clean up inputs; they are now invalid.
			arg1head.child = null;
			arg2head.child = null;
			// Merge free variable registry
			// Steal first:
			this.freevarRegistry = arg1.freevarRegistry;
			// Merge second:
			this.mergeRegistries(arg2.freevarRegistry);
			break;
		case Type.FunTerm:
			arg2 = arguments[2];
			// Build registry
			this.freevarRegistry = Object.create(null);
			var termList = [];
			var subterms = arg2;
			for (var i = 0; i < subterms.length; ++i) {
				this.mergeRegistries(subterms[i].freevarRegistry);
				termList.push(subterms[i].head.child);
			}
			// Build function and sentinel
			var template = new FreeVar(arg1, this.freevarRegistry);
			var func = new FunTerm(arg1, termList, template);
			this.head = new Sentinel(func);
			break;
		case Type.LetAbbr:
			arg2 = arguments[2];
			var abbr = new LetAbbr(arg1, arg2.head);
			this.head = new Sentinel(abbr);
			this.freevarRegistry = arg2.freevarRegistry;
			break;
		case Type.StrTerm:
			var string = new StrTerm(arg1);
			this.head = new Sentinel(string);
			this.freevarRegistry = Object.create(null);
			break;
		case Type.Int:
			var num = new Int(arg1);
			this.head = new Sentinel(num);
			this.freevarRegistry = Object.create(null);
			break;
		case Type.OpP:
			var op = new OpP(arg1,arguments[2]);
			this.head = new Sentinel(op);
			this.freevarRegistry = Object.create(null);
			break;
		default:
			this.head = new Sentinel(arguments[1]);
			this.freevarRegistry = Object.create(null);
			break;
	}
};
Term.prototype = {
	constructor: Term,
	mergeRegistries: function (registry) {
		for (var varString in registry) {
			if (! this.freevarRegistry[varString])
				this.freevarRegistry[varString] = [];
			var varLocList = registry[varString]; 
			for (var i = 0; i < varLocList.length; ++i) {
				this.freevarRegistry[varString].push(varLocList[i]);
				varLocList[i].registry = this.freevarRegistry;
			}
		}
	},
	destroy: function () {
		this.head.softDestroy();
		this.freevarRegistry = null;
		this.head = null;
	},
	contractHead: function () {
		this.head.child.contractRedex();
	},
	processAndReduce: function (condition) {
		// count the number of contractions
		var contracted = 0;
		var limit = Utility.normalizeLimit;
		
		// start with head. We will push children on the stack as we go, and we will
		// go until the stack is empty (i.e. there is no redexes remaining to
		// contract) or we have reached the limit we are willing to contract (to
		// avoid infinite loops). 
		var stack = [this.head];
		while (stack.length) {
			// Pop next node from stack; this node will be evaluated
			var cur = stack.pop();

			if (condition(cur)) {
				if (contracted >= limit) {
					return false;
				}
				var par = cur.parent;
				// If it is a redex, then:
				//  1) remove it's siblings from the stack
				var toRemove = par.children().indexOf(cur);
				stack.splice(-toRemove,toRemove);
				//  2) add its parent back on the stack to evaluate next
				// the reason for this is because contracting a redex may create a
				// redex in the parent (for example "(II)x")
				stack.push(par);
				//  3) contract
				cur.contractRedex();
				++contracted;
			}
			else {
				// If it's not a redex, then add its children
				// leftmost outermost first. 
				var children = cur.children();
				for (var i = 0; i < children.length; ++i) {
					stack.push(children[i]);
				}
			}
		}
		return true;
	},
	normalize: function () {
		return this.processAndReduce(function (a) { return a.isRedex(); });
	},
	preprocess: function () {
		return this.processAndReduce(function (a) { return a.isRedex() && a.isPreproccessed(); });
	},
	develop: function () {
		var stack = [this.head];
		var redexes = [];
		
		// Read all variables (leftmost outermost first).
		while (stack.length) {
			var cur = stack.pop();
			if (cur.isRedex())
				redexes.push(cur);
			var children = cur.children();
			for (var i = 0; i < children.length; ++i) {
				stack.push(children[i]);
			}
		}
		
		// Contract them in reverse order
		 redexes = redexes.reverse();
		 for (var i = 0; i < redexes.length; ++i) {
			redexes[i].contractRedex();
		 }
	},
	makeJQueryHTMLObject: function () {
		return this.head.makeJQueryHTMLObject();	
	}
}

Term.makeVar = function (varName) { 
	return new Term(Type.FreeVar, varName);
};
Term.makeApp = function (left,right) {
	return new Term(Type.AppTerm, left, right);
};
Term.makeAbs = function (varName,absedTerm) {
	return new Term(Type.AbsTerm, varName, absedTerm);
};
Term.makeFunction = function (varName,lamList) {
	return new Term(Type.FunTerm, varName,lamList);
};
Term.makeLetAbbr = function (varName, abbr) {
	return new Term(Type.LetAbbr, varName, abbr);
}
Term.makeString = function (value) {
	return new Term(Type.StrTerm, value);
}
Term.makeInt = function (value) {
	return new Term(Type.Int, value);
}
Term.makeOp = function (op, value) {
	return new Term(Type.OpP, op, value);
}
Term.makeConstant = function (name) {
	if (Term.constant[name] != undefined) {
		var t = Term.constant[name].head.child.cloneWithSideEffects();
		return new Term(-1, t);
	} else {
		throw new Error ("Constant " + name + " not found");
	}
}

Term.constant = { 
	"$true": Term.makeLetAbbr("$true", Term.makeAbs("x", Term.makeAbs("y", Term.makeVar("x")))),
	"$false": Term.makeLetAbbr("$false", Term.makeAbs("x", Term.makeAbs("y", Term.makeVar("y")))),
	"$I": Term.makeLetAbbr("$I", Term.makeAbs("x", Term.makeVar("x"))),
	"$K": Term.makeLetAbbr("$K", Term.makeAbs("x", Term.makeAbs("y", Term.makeVar("x")))),
	"$S": Term.makeLetAbbr("$S", parser.parse("\\x\\y\\z.x z .y z")),
	"$Y": Term.makeLetAbbr("$Y", parser.parse("\\f.(\\x.f(x x))(\\x.f(x x))")),
	"$B": Term.makeLetAbbr("$B", parser.parse("\\f\\g\\x.f.g(x)"))
};

var LetExpr = { };
LetExpr.contractLet = function (varName, replacedTerm, inTerm) {
	var abbr = Term.makeLetAbbr(varName, replacedTerm);
	inTerm = Term.makeAbs(varName, inTerm);
	var ret = Term.makeApp(inTerm, abbr);
	ret.contractHead();
	return ret;
};
LetExpr.contractFunLet = function (funDecl, replacedTerm, inTerm) {
	//funDecl = object with field .name (string) and .varlist (array of
	//strings).
	
	// Abstract out all the bound variables in `funDecl.varlist` in the
	// "correct" order 
	var varToAbstract = funDecl.varlist.reverse();
	for (var i = 0; i < varToAbstract.length; ++i) {
		replacedTerm = Term.makeAbs(varToAbstract[i], replacedTerm);
	}
	
	var name = funDecl.name;

	var funSites = inTerm.freevarRegistry[name];

	inTerm = Term.makeAbs(name, inTerm);
	inTerm = Term.makeApp(inTerm, replacedTerm);
	inTerm.contractHead();

	return inTerm;
};

